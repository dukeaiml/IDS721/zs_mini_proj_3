# IDS721 Week3 Mini-Project
## Name: Ziyu Shi, NetID: zs148

### Project description
In this project, we need to create an S3 Bucket using CDK with AWS CodeWhisperer. We should create S3 bucket using AWS CDK and use CodeWhisperer to generate CDK code. Addtionally, we need to add bucket properties like versioning and encryption.

### Experiment steps
1. Sign in [CodeCatalyst](https://codecatalyst.aws/explore). Create a project and create a new dev environment under this project.
2. Choose Cloud9 as the IDE to open the environment.
3. Create a new user in AWS IAM, attach the following policies to your new user: `IAMFullAccess`, `AmazonS3FullAccess`, `AWSLambda_FullAccess`, `AdministratorAccess`.
4. After creating the user, add inline policies: `Cloud Formation`, `Systems Manager`. Select all access.
5. Generate access key for your new user and save the keys.
6. use `aws configure` in Cloud9 IDE to set up your access key and region.
7. `mkdir YOUR_PROJECT_NAME` and `cd` into it. Use the following command to create the template.
    ```
    cdk init app --language=typescript
    ``` 
8. Click the AWS logo in the sidebar to find CodeWhisperer in the Developer tools. Make sure to enable it.
9. Use Codewhisperer to generate code: I use the `// make an S3 bucket that enable versioning and encryption` in the `/lib/mini_proj3-statck.ts` to generate code enabling versioning and encryption, use the `// add necessary variables to create the S3 bucket and deploy it` in the `/bin/mini_proj3.ts` to generate code creating a S3 bucket for me.
10. After using CodeWhisperer to generate code, use following command to compile ts file.
    ```
    npm run build
    ```
11. Then use following command to create CloudFormation template.
    ```
    cdk synth
    ```
12. Then deploy the template by the following command.
    ```
    cdk deploy
    ```
    If errors occur, try `cdk bootstrap` before `cdk deploy`.
13. Navigate to the AWS account and check the S3 bucket and its properties.

### Screenshots of the new bucket
1. **The new bucket**
    ![new bucket](/images/bucket.png)

2. **The enabled versioning**
    ![versioning](/images/versioning.png)

3. **The enabled encryption**
    ![encription](/images/encription.png)

### The usage of CodeWhisperer
Use Codewhisperer to generate code: 

In `/lib/mini_proj3-statck.ts`, I use the prompt of `// make an S3 bucket that enable versioning and encryption` to generate code enabling versioning and encryption. The encryption method I utilized is KMS.

In `/bin/mini_proj3.ts`, I use the prompt of `// add necessary variables to create the S3 bucket and deploy it` to generate code creating a S3 bucket for me.